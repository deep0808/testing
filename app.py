from flask import *
import pyrebase


config = {
	"apiKey": "AIzaSyD0fzFslus_LRDNQI022QHTAJ5Ch0vgpZ0",
    "authDomain": "akrgtesting.firebaseapp.com",
    "databaseURL": "https://akrgtesting-default-rtdb.firebaseio.com",
    "projectId": "akrgtesting",
    "storageBucket": "akrgtesting.appspot.com",
    "messagingSenderId": "557661419296",
    "appId": "1:557661419296:web:e0e73026a6e2a2770a4945",
    "measurementId": "G-Y6P5JBQ7SV"
}

firebase = pyrebase.initialize_app(config)
db = firebase.database()

app = Flask(__name__)

app.secret_key = "hellosudeep"

@app.route("/")
def home():
	return render_template('home.html')

@app.route("/check")
def check():
	return render_template('final.html')

@app.route("/final", methods = ['GET','POST'])
def final():
	if request.method == 'POST':
		regid = request.form['registerno']
		semid = request.form['semid']
		res = db.child(regid).get()

		for task in res.each():
			if task.val()['Sem'] == semid:
				res = task.val()
		return render_template('res.html',marks = res)

	return render_template('home.html')

@app.route('/enter')
def enter():
	return render_template('s1.html')

@app.route('/sone',methods = ['GET','POST'])
def sone():
	if request.method == 'POST':
		regno = request.form['regno']
		sem = request.form['sem']
		session['regno'] = regno
		session['sem'] = sem
		if sem == '1-1':
			return render_template('oo.html')
		elif sem == '1-2':
			return render_template('ot.html')
		elif sem == '2-1':
			return render_template('to.html')
		elif sem == '2-2':
			return render_template('tt.html')
		elif sem == '3-1':
			return render_template('tho.html')
		elif sem == '3-2':
			return render_template('tht.html')
		elif sem == '4-1':
			return render_template('fo.html')
		elif sem == '4-2':
			return render_template('ft.html')
	return render_template('s1.html')

@app.route('/oo', methods = ['GET','POST'])
def oo():
	if request.method == 'POST':
		regno = session['regno']
		sem = session['sem']
		e1 = request.form['e1']
		m1 = request.form['m1']
		m2 = request.form['m2']
		ap = request.form['ap']
		cp = request.form['cp']
		ed  = request.form['ed']
		ecl = request.form['ecl']
		pl = request.form['pl']
		cpl = request.form['cpl']
		data =  { 'Regno': regno,
          'Sem': sem,
          'english-1': e1,
          'mathematics-1':m1,
          'mathematics-2':m2,
          'applied-physics':ap,
          'computer programming':cp,
          'engineering drawing':ed,
          'englishb communications lab':ecl,
          'physics lab':pl,
          'computer programming lab':cpl
          }
		result = db.child(regno).push(data)
	return render_template('sone.html')

@app.route('/ot', methods = ['GET','POST'])
def ot():
	if request.method == 'POST':
		regno = session['regno']
		sem = session['sem']
		e1 = request.form['e1']
		m1 = request.form['m1']
		m2 = request.form['m2']
		ap = request.form['ap']
		cp = request.form['cp']
		ed  = request.form['ed']
		ecl = request.form['ecl']
		pl = request.form['pl']
		cpl = request.form['cpl']
		data =  { 'Regno': regno,
          'Sem': sem,
          'english-1': e1,
          'mathematics-1':m1,
          'mathematics-2':m2,
          'applied-physics':ap,
          'computer programming':cp,
          'engineering drawing':ed,
          'englishb communications lab':ecl,
          'physics lab':pl,
          'computer programming lab':cpl
          }
		result = db.child(regno).push(data)
	return render_template('sone.html')

if __name__ == "__main__":
	app.run(debug = True)
